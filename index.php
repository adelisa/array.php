<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("Hewan -AnyScr-");
echo "Name: " . $sheep->name . "<br>";
echo "Legs: " . $sheep->legs . "<br>";
echo "Cold blooded: " . $sheep->cold_blooded . "<br><br>";

$kodok = new Frog("Katak");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold blooded: " . $kodok->cold_blooded . "<br>";
$kodok->jump();
echo "<br><br>";

$sungokong = new Ape("Monyet");
echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legss . "<br>";
echo "Cold blooded: " . $sungokong->cold_blooded . "<br>";
$sungokong->yell();

?>